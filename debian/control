Source: ipod-sharp
Section: cli-mono
Priority: optional
Maintainer: Debian CLI Libraries Team <pkg-cli-libs-team@lists.alioth.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Chow Loong Jin <hyperair@ubuntu.com>
Build-Depends: debhelper (>= 7.0.50)
Build-Depends-Indep: cli-common-dev (>= 0.5.7),
                     mono-devel (>= 2.4.3),
                     libndesk-dbus1.0-cil-dev,
                     libndesk-dbus-glib1.0-cil-dev,
                     libgtk2.0-cil-dev,
                     pkg-config,
                     monodoc-base (>= 1.1.9),
                     podsleuth (>= 0.6.0)
Standards-Version: 3.8.3
Vcs-Browser: http://git.debian.org/?p=pkg-cli-libs/packages/ipod-sharp.git
Vcs-Git: git://git.debian.org/git/pkg-cli-libs/packages/ipod-sharp.git

Package: libipod-cil
Architecture: all
Depends: ${cli:Depends},
         ${misc:Depends}
Recommends: podsleuth (>= 0.6.0)
Description: CLI library for accessing iPods
 ipod-sharp is a library that allows manipulation of the iTunesDB used in
 Apple iPod devices. Currently it supports adding/removing songs and
 manipulating playlists.

Package: libipod-cil-dev
Architecture: all
Depends: libipod-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libipod-cil (<< 0.8.5-1)
Recommends: podsleuth (>= 0.6.0)
Description: CLI library for accessing iPods - development files
 ipod-sharp is a library that allows manipulation of the iTunesDB used in
 Apple iPod devices. Currently it supports adding/removing songs and
 manipulating playlists.
 .
 This package contains the development files for the ipod-sharp libraries, and
 should be used for compilation.

Package: libipodui-cil
Architecture: all
Depends: ${cli:Depends},
         ${misc:Depends},
         libipod-cil (= ${binary:Version})
Description: CLI library for accessing iPods (GUI helpers)
 ipod-sharp is a library that allows manipulation of the iTunesDB used in
 Apple iPod devices. Currently it supports adding/removing songs and
 manipulating playlists.

Package: libipodui-cil-dev
Architecture: all
Depends: libipodui-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libipod-cil (<< 0.8.5-1)
Recommends: podsleuth (>= 0.6.0)
Description: CLI library for accessing iPods (GUI helpers) - development files
 ipod-sharp is a library that allows manipulation of the iTunesDB used in
 Apple iPod devices. Currently it supports adding/removing songs and
 manipulating playlists.
 .
 This package contains the development files for the ipod-sharp UI libraries,
 and should be used for compilation.

Package: monodoc-ipod-manual
Section: doc
Architecture: all
Depends: monodoc-manual,
         ${misc:Depends}
Description: compiled XML documentation for ipod-sharp
 The MonoDoc Project is the documentation framework of the Mono project which
 provides detailed API documentation for all Mono components and the Mono CLI
 implementation.
 .
 This package contains the compiled XML documentation for
 ipod-sharp of MonoDoc.

